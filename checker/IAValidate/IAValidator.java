
import org.xdu.jia.*;
import java.io.*;

public class IAValidator{
	public static void user_comp(String pathname){
		File f=new File(pathname);
		if(!f.exists()) {
			System.out.println("File <" + pathname + "> does not exist.");
			return;
		}
		
		/* the original example in de Alfaro 2001*/
		InterfaceAutomaton User=new InterfaceAutomaton("IA [s0, s1] (s0) [fail, ok] [msg] [] " +
				"[s0->(msg)s1, s1->(ok)s0]");
		InterfaceAutomaton Comp=new InterfaceAutomaton("IA [s0,s1,s2,s3,s4,s5,s6] (s0) " +
				"[msg,ack,nack] [fail,ok,send] [] " +
				"[s0->(msg)s1, s1->(send)s2, s2->(nack)s3, s3->(send)s4, s4->(ack)s5, " +
				"s2->(ack)s5, s5->(ok)s0, s4->(nack)s6, s6->(fail)s0]");
		/*test the parser*/
		IAParser parser=new IAParser();
		parser.ConstructIA(pathname);
		
		for(InterfaceAutomaton ia: parser.getInterfaceAutomatons()){
			String ia_name = ia.getName();
			
			System.out.println("IA(" + ia_name + ") valid: " + ia.bValid());
			if(ia_name.equals("User"))
				System.out.println("IA(" + ia_name + ") equivalent: " + ia.Equivalence(User));
			else if(ia_name.equals("Comp"))
				System.out.println("IA(" + ia_name + ") equivalent: " + ia.Equivalence(Comp));
		}

		InterfaceAutomaton user_comp_prod = User.ReducedProduct(Comp);
		System.out.println("User*Comp reduced product: ");
		user_comp_prod.Print();
		
		InterfaceAutomaton tmp=new InterfaceAutomaton("IA [s1_s1, s0_s0, s1_s2, s1_s5, s1_s3, s1_s4, s1_s6] (s0_s0) " +
				"[nack, ack] [send] [msg, ok, fail] [s1_s4->(ack)s1_s5, s1_s4->(nack)s1_s6, s1_s5->(ok)s0_s0, s1_s3->(send)s1_s4," +
				" s1_s2->(nack)s1_s3, s1_s2->(ack)s1_s5, s1_s1->(send)s1_s2, s0_s0->(msg)s1_s1]");
		System.out.println("User*Comp ReducedProduect passed: " + user_comp_prod.Equivalence(tmp));
		
		System.out.println("User & Comp compatible: " + User.bCompatible(Comp) );
		
		InterfaceAutomaton user_comp=User.Composition(Comp);
		//System.out.println(user_comp.generateIAOutputString("user_comp") );
		user_comp.Print();
		tmp=new InterfaceAutomaton("IA [s1_s4, s1_s3, s1_s5, s1_s2, s0_s0, s1_s1] (s0_s0) [nack, ack] [send] [msg, ok, fail]" +
				" [s0_s0->(msg)s1_s1, s1_s1->(send)s1_s2, s1_s2->(ack)s1_s5, s1_s2->(nack)s1_s3," +
				" s1_s3->(send)s1_s4, s1_s5->(ok)s0_s0, s1_s4->(ack)s1_s5]");
		System.out.println("User||Comp equivalent: "+user_comp.Equivalence(tmp));
		System.out.println("User||Comp equivalent to User||_<2>Comp : "+ user_comp.Equivalence(User.Composition2(Comp)));
		
		InterfaceAutomaton Channel=new InterfaceAutomaton("IA [s0,s1,s2,s3] (s0) " +
				"[send] [ack,nack,gettoken,puttoken] []	" +
				"[s0->(send)s1, s1->(gettoken)s2, s2->(ack)s3, s3->(puttoken)s0]");
		System.out.println("User||Comp & Channel compatible: " + user_comp.bCompatible(Channel));
		
		System.out.println("(User*Comp) * Channel reduced product: ");
		user_comp_prod.ReducedProduct(Channel).Print();
	}
	
	public static void message_transmission_service(String pathname){
		/* the examples of de Alfaro 2005*/
		File f=new File(pathname);
		if(!f.exists()) {
			System.out.println("File <" + pathname + "> does not exist.");
			return;
		}
		
		InterfaceAutomaton TryTwice = null;
		InterfaceAutomaton OnceOrTwice = null;
		InterfaceAutomaton IllOnceOrTwice = null;
		InterfaceAutomaton Client = null;

		IAParser parser=new IAParser();
		parser.ConstructIA(pathname);
		for(InterfaceAutomaton ia: parser.getInterfaceAutomatons()){
			String ia_name = ia.getName();
			
			System.out.println("IA(" + ia_name + ") valid: " + ia.bValid());
			if(ia_name.equals("TryTwice"))
				TryTwice = ia;
			if(ia_name.equals("OnceOrTwice"))
				OnceOrTwice = ia;
			if(ia_name.equals("IllOnceOrTwice"))
				IllOnceOrTwice = ia;
			if(ia_name.equals("Client"))
				Client = ia;
		}
		if (TryTwice == null || OnceOrTwice == null || IllOnceOrTwice == null || Client == null) {
			System.out.println("Automata unresolved");
			return;
		}
		
		System.out.println("TryTwice*Client reduced product: ");
		System.out.println(TryTwice.ReducedProduct(Client).generateIAOutputString("TryTwice*Client"));
		
		System.out.println("TryTwice & Client compatible: " + TryTwice.bCompatible(Client));
		
		InterfaceAutomaton trytwice_client = TryTwice.Composition(Client);
		System.out.println(trytwice_client.generateIAOutputString("TryTwice||Client"));
		System.out.println("TryTwice||Client equivalent to TryTwice||_<2>Client : " + trytwice_client.Equivalence(TryTwice.Composition2(Client)));

		System.out.println("TryTwice > OnceOrTwice : " + TryTwice.bRefinedBy(OnceOrTwice));
		System.out.println("TryTwice >_2 OnceOrTwice : " + TryTwice.bRefinedBy2(OnceOrTwice));
		
		System.out.println("TryTwice > IllOnceOrTwice : " + TryTwice.bRefinedBy(IllOnceOrTwice));
		System.out.println("TryTwice >_2 IllOnceOrTwice : " + TryTwice.bRefinedBy2(IllOnceOrTwice));
	}
	
	public static void prod_pay(String pathname){
		/*From Esmaeilsabzali, S., et al. (2006). "Interface Automata with Complex Actions." ENTCS 159: 79-97.*/
		File f=new File(pathname);
		if(!f.exists()) {
			System.out.println("File <" + pathname + "> does not exist.");
			return;
		}
		
		InterfaceAutomaton Prod = null;
		InterfaceAutomaton Pay = null;
		InterfaceAutomaton GenPay = null;
		
		IAParser parser=new IAParser();
		parser.ConstructIA(pathname);
		for(InterfaceAutomaton ia: parser.getInterfaceAutomatons()){
			String ia_name = ia.getName();
			
			System.out.println("IA(" + ia_name + ") valid: " + ia.bValid());
			if(ia_name.equals("Prod"))
				Prod = ia;
			if(ia_name.equals("Pay"))
				Pay = ia;
			if(ia_name.equals("GenPay"))
				GenPay = ia;
		}
		if (Prod == null || Pay == null || GenPay == null) {
			System.out.println("Automata unresolved.");
			return;
		}
		
		System.out.println(Prod.ReducedProduct(Pay).generateIAOutputString("Prod*Pay"));
		
		System.out.println("Prod & Pay compatible: " + Prod.bCompatible(Pay));
		
		InterfaceAutomaton prod_pay = Prod.Composition(Pay);
		System.out.println(prod_pay.generateIAOutputString("Prod||Pay"));
		
		System.out.println("Prod||Pay equivalent to Prod||_<2>Pay: " + prod_pay.Equivalence(Prod.Composition2(Pay)));
		
		
		InterfaceAutomaton prod_genpay_prod = Prod.ReducedProduct(GenPay);
		System.out.println(prod_genpay_prod.generateIAOutputString("Prod*GenPay"));
		
		System.out.println("Prod & GenPay compatible: " + Prod.bCompatible(GenPay));
		
		InterfaceAutomaton prod_genpay = Prod.Composition(GenPay);
		System.out.println(prod_genpay.generateIAOutputString("Prod||GenPay"));
		
		System.out.println("Prod||GenPay equivalent to Prod||_<2>GenPay: " + prod_genpay.Equivalence(Prod.Composition2(GenPay)));
		
		
		System.out.println("Pay > GenPay: " + Pay.bRefinedBy(GenPay));
		System.out.println("Pay >_2 GenPay: " + Pay.bRefinedBy2(GenPay) );
		
		System.out.println("Prod||Pay > Prod||GenPay: " + prod_pay.bRefinedBy(prod_genpay));
		System.out.println("Prod||Pay >_2 Prod||GenPay: " + prod_pay.bRefinedBy2(prod_genpay));
	}
	
	public static void bufferd_receiver(String pathname){
		/* In de Alfaro, L. and M. Stoelinga (2004). 
		 * "Interfaces: A Game-Theoretic Framework for Reasoning About Component-Based Systems."
		 * Electronic Notes in Theoretical Computer Science 97: 3-23.*/
		
		File f=new File(pathname);
		if(!f.exists()) {
			System.out.println("File <" + pathname + "> does not exist.");
			return;
		}
		
		InterfaceAutomaton Buffer=null;
		InterfaceAutomaton Receiver=null;
		
		IAParser parser=new IAParser();
		parser.ConstructIA(pathname);
		
		for(InterfaceAutomaton ia: parser.getInterfaceAutomatons()){
			String ia_name = ia.getName();
			
			System.out.println("IA(" + ia_name + ") valid: " + ia.bValid());
			if(ia_name.equals("Buffer"))
				Buffer=ia;
			if(ia_name.equals("Receiver"))
				Receiver=ia;
		}
		if (Buffer == null || Receiver == null) {
			System.out.println("Buffer or Receiver unresolved.");
			return;
		}
		
		InterfaceAutomaton buffer_receiver_prod = Buffer.ReducedProduct(Receiver);
		System.out.println(buffer_receiver_prod.generateIAOutputString("Buffer*Receiver"));
		
		InterfaceAutomaton tmp=new InterfaceAutomaton("IA [b1_r0, b0_r0, b2_r0, b0_r1, b1_r1, b2_r1] (b0_r0) [snd] [proc] [rec] " +
				"[b2_r1->(proc)b2_r0, b1_r1->(proc)b1_r0, b1_r1->(snd)b2_r1, b2_r0->(rec)b1_r1, b0_r1->(proc)b0_r0, " +
				"b0_r1->(snd)b1_r1, b1_r0->(rec)b0_r1, b1_r0->(snd)b2_r0, b0_r0->(snd)b1_r0]");
		System.out.println("Buffer*Receiver ReducedProduct passed:"+ buffer_receiver_prod.Equivalence(tmp));
		
		System.out.println("Buffer & Receiver compatible: " + Buffer.bCompatible(Receiver));
		
		InterfaceAutomaton buffer_receiver = Buffer.Composition(Receiver);
		System.out.println(buffer_receiver.generateIAOutputString("Buffer||Receiver"));
		
		tmp=new InterfaceAutomaton("IA [b0_r1, b0_r0, b1_r0] (b0_r0) [snd] [proc] [rec] " +
				"[b0_r0->(snd)b1_r0, b1_r0->(rec)b0_r1, b0_r1->(proc)b0_r0]");
		System.out.println("Buffer||Receiver equivalent: " + buffer_receiver.Equivalence(tmp));
		System.out.println("Buffer||Receiver equivalent to Buffer||_<2>Receiver: " + buffer_receiver.Equivalence(Buffer.Composition2(Receiver)));
	}
	
	public static void datalink_client(String pathname){
		/*this example is from Larsen K., et al. Modal I/O Automata for Interface and Product Line Theories.*/
		
		File f=new File(pathname);
		if(!f.exists()) {
			System.out.println("File <" + pathname + "> does not exist.");
			return;
		}
		
		InterfaceAutomaton Datalink = null;
		InterfaceAutomaton Client = null;

		IAParser parser=new IAParser();
		parser.ConstructIA(pathname);
		
		for(InterfaceAutomaton ia: parser.getInterfaceAutomatons()){
			String ia_name = ia.getName();
			
			System.out.println("IA(" + ia_name + ") valid: " + ia.bValid());
			if(ia_name.equals("Datalink"))
				Datalink = ia;
			if(ia.getName().equals("Client"))
				Client = ia;
		}
		if(Datalink == null || Client == null) {
			System.out.println("Automata unresolved.");
			return;
		}
		
		InterfaceAutomaton datalink_client_prod = Datalink.ReducedProduct(Client);
		System.out.println(datalink_client_prod.generateIAOutputString("Datalink*Client"));
		
		InterfaceAutomaton tmp=new InterfaceAutomaton("IA [s15_t3, s14_t2, s16_t3, s19_t3, s17_t3, s20_t3, s18_t3, s21_t3, s22_t3] (s14_t2) " +
				"[down, up, nack, ack] [log, linkStatus, trnsmt] [fail, ok, send] " +
				"[s21_t3->(log)s22_t3, s18_t3->(nack)s17_t3, s18_t3->(ack)s19_t3, s20_t3->(up)s17_t3, s20_t3->(down)s21_t3, s19_t3->(ok)s14_t2, " +
				"s17_t3->(trnsmt)s18_t3, s17_t3->(linkStatus)s20_t3, s16_t3->(nack)s17_t3, s16_t3->(ack)s19_t3, s15_t3->(trnsmt)s16_t3, s14_t2->(send)s15_t3]");
		System.out.println("Datalink*Client ReducedProduct passed:"+ datalink_client_prod.Equivalence(tmp));
		
		System.out.println("Datalink & Client compatible: " + Datalink.bCompatible(Client));
		
		InterfaceAutomaton datalink_client = Datalink.Composition(Client);
		System.out.println(datalink_client.generateIAOutputString("Datalink||Client"));
		
		InterfaceAutomaton datalink_client2 = Datalink.Composition2(Client);
		System.out.println(datalink_client2.generateIAOutputString("Datalink||_<2>Client"));
		
		tmp=new InterfaceAutomaton("IA [s18_t3, s20_t3, s17_t3, s19_t3, s16_t3, s14_t2, s15_t3] (s14_t2) " +
				"[down, up, nack, ack] [log, linkStatus, trnsmt] [fail, ok, send] " +
				"[s14_t2->(send)s15_t3, s15_t3->(trnsmt)s16_t3, s16_t3->(ack)s19_t3, s16_t3->(nack)s17_t3, s17_t3->(linkStatus)s20_t3, " +
				"s17_t3->(trnsmt)s18_t3, s19_t3->(ok)s14_t2, s20_t3->(up)s17_t3, s18_t3->(ack)s19_t3, s18_t3->(nack)s17_t3]");
		System.out.println("Datalink||Client equivalent: " + datalink_client.Equivalence(tmp));
		System.out.println("Datalink||Client equivalent to Datalink||_<2>Client: " + datalink_client.Equivalence(datalink_client2));
	}
	
	public static void tran_proc(String pathname){
		/*the following is the example of Lee's ENTCS 264(2010)*/
		File f=new File(pathname);
		if(!f.exists()) {
			System.out.println("File <" + pathname + "> does not exist.");
			return;
		}
		InterfaceAutomaton TS = null;
		InterfaceAutomaton TPU = null;
		InterfaceAutomaton Sup = null;
		
		IAParser parser=new IAParser();
		parser.ConstructIA(pathname);
		for(InterfaceAutomaton ia: parser.getInterfaceAutomatons()){
			String ia_name = ia.getName();
			
			System.out.println("IA(" + ia_name + ") valid: " + ia.bValid());
			if(ia_name.equals("TS"))
				TS = ia;
			if(ia_name.equals("TPU"))
				TPU = ia;
			if(ia_name.equals("Sup"))
				Sup = ia;
		}
		if(TS == null || TPU == null || Sup == null) {
			System.out.println("Automata unresolved");
			return;
		}
		
		InterfaceAutomaton ts_tpu_prod = TS.ReducedProduct(TPU);
		System.out.println(ts_tpu_prod.generateIAOutputString("TS*TPU"));
		
		InterfaceAutomaton tmp=new InterfaceAutomaton("IA [s4_t1, s1_t1, s2_t1, s5_t1, s3_t2, s3_t4, s3_t3, s6_t2, s6_t4, s6_t3, s7_t1] (s1_t1)" +
				" [startM, endM, newT, acceptT] [logF, ok, nOk, logM] [startT, endT]" +
				" [s7_t1->(logM)s4_t1, s6_t3->(endT)s7_t1, s6_t4->(logF)s6_t3, s3_t3->(endT)s1_t1, s6_t2->(ok)s6_t3," +
				" s6_t2->(nOk)s6_t4, s3_t4->(logF)s3_t3, s5_t1->(startT)s6_t2, s3_t2->(ok)s3_t3, s3_t2->(nOk)s3_t4," +
				" s2_t1->(startT)s3_t2, s4_t1->(endM)s1_t1, s4_t1->(newT)s5_t1, s1_t1->(acceptT)s1_t1," +
				" s1_t1->(newT)s2_t1, s1_t1->(startM)s4_t1]");
		System.out.println("TS*TPU ReducedProduct passed: " + ts_tpu_prod.Equivalence(tmp));
		
		System.out.println("TS & TPU compatible : " + TS.bCompatible(TPU));
		
		InterfaceAutomaton c=TS.Composition(TPU);
		System.out.println(c.generateIAOutputString("TS||TPU"));
		System.out.println("TS||TPU equivalent to TS||_<2>TPU : " + c.Equivalence(TS.Composition2(TPU)));
		
		System.out.println("TS||TPU & Sup compatible: " + c.bCompatible(Sup));
		
		InterfaceAutomaton res = c.Composition(Sup);
		System.out.println(res.generateIAOutputString("TS||TPU||Sup"));//same as the paper
		System.out.println("(TS||TPU)||Sup equivalent to (TS||_<2>TPU)||_<2>Sup:" + 
							res.Equivalence(TS.Composition2(TPU).Composition2(Sup)));
	}
	

	
	
	public static void main(String[] args){
		//user_comp("../../examples/user_comp.si");
		//message_transmission_service("../../examples/mts.si");
		prod_pay("../../examples/prod_pay.si");
		//tran_proc("../../examples/tran_proc.si");
		//bufferd_receiver("../../examples/buffered_receiver.si");
		//datalink_client("../../examples/datalink_client.si");
	}
}