/*
 * This software was developed by Cong Sun <suncong@xidian.edu.cn>
 * at Xidian University, China.
 *
 * This file is part of Open Interface Automata.
 * Open Interface Automata is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Open Interface Automata is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Open Interface Automata.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.xdu.jia;

import org.xdu.jia.parser.parser.*;
import org.xdu.jia.parser.lexer.*;
import org.xdu.jia.parser.node.*;
import org.xdu.jia.parser.analysis.*;
import java.io.*;
import java.util.*;

class Translation extends DepthFirstAdapter{
	List<InterfaceAutomaton> ialist=new ArrayList<InterfaceAutomaton>();
	private static int idx=0; 
	private Set<Transition> tmpTrans=new HashSet<Transition>();
	private Action tmpAct=null;
	
	public List<InterfaceAutomaton> getInterfaceAutomatons(){
		return ialist;
	}
	
	/*the overrided methods */
    public void inAModuledecl(AModuledecl node)
    {
        ialist.add(idx, new InterfaceAutomaton());
    }

    public void outAModuledecl(AModuledecl node)
    {
    	//ialist.get(idx).Print();
        idx++;
    }

    public void outAModname(AModname node)
    {
    	ialist.get(idx).setName(node.getId().getText());
    }

    public void outAInitsetd(AInitsetd node)
    {
    	ialist.get(idx).setInit(node.getId().getText());
    }

    public void inAOutputTransition(AOutputTransition node)
    {
        tmpTrans.clear();
    }

    public void outAOutputTransition(AOutputTransition node)
    {
    	ialist.get(idx).addTransitions(tmpTrans);
    	ialist.get(idx).addOutAction(tmpAct);
    	tmpTrans.clear();
    }

    public void inAInputTransition(AInputTransition node)
    {
        tmpTrans.clear();
    }

    public void outAInputTransition(AInputTransition node)
    {
    	ialist.get(idx).addTransitions(tmpTrans);
    	ialist.get(idx).addInAction(tmpAct);
    	tmpTrans.clear();
    }

    public void inAHideTransition(AHideTransition node)
    {
        tmpTrans.clear();
    }

    public void outAHideTransition(AHideTransition node)
    {
    	ialist.get(idx).addTransitions(tmpTrans);
    	ialist.get(idx).addHideAction(tmpAct);
    	tmpTrans.clear();
    }

    public void outAActname(AActname node)
    {
    	tmpAct=new Action(node.getId().getText());
    }

    public void outAStmt(AStmt node)
    {
    	ialist.get(idx).addState(node.getLeft().getText());
    	ialist.get(idx).addState(node.getRight().getText());
    	tmpTrans.add(new Transition(node.getLeft().getText(), tmpAct.getStr() ,node.getRight().getText()));
    }

}

public class IAParser{
	List<InterfaceAutomaton> ialist=new ArrayList<InterfaceAutomaton>();
	
	/**
	 * @return the automata derived in the parsing phase
	 */
	public List<InterfaceAutomaton> getInterfaceAutomatons(){
		return ialist;
	}
	
	/**
	 * parse and generate a set of automata to &lt;ialist&gt;
	 * @param pathname the path name of the source file
	 */
	public void ConstructIA(String pathname) {
		try {
			ialist.clear();
			Parser p = new Parser(new Lexer(new PushbackReader(
				new FileReader(pathname), 1024)));
			Start tree = p.parse();
			Translation tl=new Translation();
			tree.apply(tl);
			ialist=tl.getInterfaceAutomatons();
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}
}
