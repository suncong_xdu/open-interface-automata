/*
 * This software was developed by Cong Sun <suncong@xidian.edu.cn>
 * at Xidian University, China.
 *
 * This file is part of Open Interface Automata.
 * Open Interface Automata is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Open Interface Automata is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Open Interface Automata.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.xdu.jia;

import java.util.*;

public class InterfaceAutomaton{
	private String name; //used by the parser
	private State init;
	private Set<State> states=new HashSet<State>();
	private Set<Action> ins=new HashSet<Action>();
	private Set<Action> outs=new HashSet<Action>();
	private Set<Action> hides=new HashSet<Action>();
	private Set<Transition> trans=new HashSet<Transition>();
	
	/**
	 * the constructor used for testing
	 */
	public InterfaceAutomaton(String[] states, String init, String[] ins, String[] outs, String[] hides, String[] trans){
		this.init=new State(init);
		for(String s: states){
			this.states.add(new State(s));
		}
		for(String a: ins){
			this.ins.add(new Action(a));
		}
		for(String a: outs){
			this.outs.add(new Action(a));
		}
		for(String a: hides){
			this.hides.add(new Action(a));
		}
		for(String tr: trans){
			StringTokenizer st=new StringTokenizer(tr,"(),");
			this.trans.add(new Transition(st.nextToken(),st.nextToken(),st.nextToken()) );
		}
	}
	
	/**
	 * the constructor taking certia's IA string
	 */
	public InterfaceAutomaton(String s_ia){
		StringTokenizer st=new StringTokenizer(s_ia,"[");
		StringTokenizer st2;
		int idx=0;
		while(st.hasMoreTokens()){
			String tmp=st.nextToken();
			if((tmp.trim()).startsWith("IA"))
				continue;
			if((tmp.replaceAll(" ","")).equals(""))
				continue;
			tmp.trim();
			if(idx==0){
				String[] s= tmp.split("]");
				//System.out.println("s[0]:"+s[0]);
				
				st2=new StringTokenizer(s[0],",");
				while(st2.hasMoreTokens()){
					this.states.add(new State(st2.nextToken().trim()));
				}
				st2=new StringTokenizer(s[1].trim(),"()");
				if(st2.hasMoreTokens()){
					String tmp_init=st2.nextToken();
					if(! bValidState(tmp_init))
						System.out.println("Invalid initial state");
					else
						this.init=new State(tmp_init);
				}
			}
			else if(idx==1){ //ins
				st2=new StringTokenizer(tmp,",]");
				while(st2.hasMoreTokens()){
					String s=st2.nextToken().trim();
					if(s.equals(""))
						continue;
					this.ins.add(new Action(s));
				}
			}
			else if(idx==2){ //outs
				st2=new StringTokenizer(tmp,",]");
				while(st2.hasMoreTokens()){
					String s=st2.nextToken().trim();
					if(s.equals(""))
						continue;
					this.outs.add(new Action(s));
				}
			}
			else if(idx==3){ //hides
				st2=new StringTokenizer(tmp,",]");
				while(st2.hasMoreTokens()){
					String s=st2.nextToken().trim();
					if(s.equals(""))
						continue;
					this.hides.add(new Action(s));
				}
			}
			else if(idx==4){//transitions
				st2=new StringTokenizer(tmp,",]");
				while(st2.hasMoreTokens()){
					String s=st2.nextToken().trim();
					//System.out.println(s);
					int k=s.indexOf("->(");
					if(k==-1){
						System.out.println("Invalid transition");
					}
					else{
						String s1=s.substring(0,k);
						s=s.substring(k+3);
						int j=s.indexOf(")");
						if(j==-1){
							System.out.println("Invalid transition");
						}
						else{
							String a=s.substring(0,j);
							s=s.substring(j+1);
							
/*							if(! bValidState(s1) || !bValidState(s) || !bValidAction(a)){
								System.out.println("Invalid state or action for transition");
							}*/
							if(! bValidState(s1))
								System.out.println(s1+" not a valid state");
							if(! bValidState(s))
								System.out.println(s+" not a valid state");
							if(! bValidAction(a)){
								System.out.println(a+" not a valid action");
							}
							this.trans.add(new Transition(s1, a, s));
						}
					}
				}
			}
			idx++;
		}
	}
	
	public InterfaceAutomaton(){
	}
	
	/*used in the parser*/
	public void setName(String s){
		this.name=s;
	}
	public String getName(){
		return this.name;
	}
	
	public void setInit(String s){
		this.init=new State(s);
	}
	
	public void addTransitions(Set<Transition> trs){
		this.trans.addAll(trs);
	}
	
	public void addTransition(Transition tr){
		this.trans.add(tr);
	}
	
	public void addOutAction(Action a){
		this.outs.add(a);
	}
	
	public void addInAction(Action a){
		this.ins.add(a);
	}
	
	public void addHideAction(Action a){
		this.hides.add(a);
	}
	
	public void addState(String s){
		this.states.add(new State(s));
	}
	
	/**
	 * @param ia a specific automaton
	 * @return whether the current automaton is equivalent to the automaton ia
	 */
	public boolean Equivalence(InterfaceAutomaton ia){
		if(!this.init.equals(ia.init)){
			System.out.println("initial state inequivalent");
			return false;
		}
		if(!this.states.containsAll(ia.states) || ! ia.states.containsAll(this.states)){
			System.out.println("sets of states inequivalent");
			return false;
		}
		if(!this.ins.containsAll(ia.ins) || ! ia.ins.containsAll(this.ins)){
			System.out.println("sets of input actions inequivalent");
			return false;
		}
		if(!this.outs.containsAll(ia.outs) || ! ia.outs.containsAll(this.outs)){
			System.out.println("sets of output actions inequivalent");
			return false;
		}
		if(!this.hides.containsAll(ia.hides) || !ia.hides.containsAll(this.hides)){
			System.out.println("sets of hide actions inequivalent");
			return false;
		}
		if(!this.trans.containsAll(ia.trans) || !ia.trans.containsAll(this.trans)){
			System.out.println("sets of transitions inequivalent");
			return false;
		}
		return true;
	}
	
	/**
	 * @param st a string for a state name
	 * @return whether there is any state st in the current automaton
	 */
	private boolean bValidState(String st){
		boolean res=false;
		for(State s: this.states){
			if(s.getStr().equals(st)){
				res=true;
				break;
			}
		}
		return res;
	}
	
	/**
	 * @param ac a string for an action name
	 * @return whether there is any action a in the current automaton
	 */
	private boolean bValidAction(String ac){
		for(Action a: this.ins){
			if(a.getStr().equals(ac))
				return true;
		}
		for(Action a: this.outs){
			if(a.getStr().equals(ac))
				return true;
		}
		for(Action a: this.hides){
			if(a.getStr().equals(ac))
				return true;
		}
		return false;
	}
	
	/*
	 * Getters
	 */
	public State getInit(){
		return init;
	}
	
	public Set<State> getStates(){
		return states;
	}
	
	public Set<Action> getActions(){
		Set<Action> s=new HashSet<Action>();
		s.addAll(ins);
		s.addAll(outs);
		s.addAll(hides);
		return s;
	}
	
	public Set<Action> getInActions(){
		return ins;
	}
	
	public Set<Action> getOutActions(){
		return outs;
	}
	
	public Set<Action> getHideActions(){
		return hides;
	}
	
	public Set<Transition> getTransitions(){
		return trans;
	}
	
	private Set<Action> getActionsInSet(State q, Set<Action> acts){
		Set<Action> res=new HashSet<Action>();
		for(Transition tran: this.trans){
			if(tran.getFr().equals(q) && acts.contains(tran.getAct() ) )
				res.add(tran.getAct());
		}
		return res;
	}
	
	public Set<Action> getInActions(State q){
		return getActionsInSet(q,ins);
	}
	
	public Set<Action> getOutActions(State q){
		return getActionsInSet(q,outs);
	}
	
	public Set<Action> getHideActions(State q){
		return getActionsInSet(q,hides);
	}
	
	public Set<Action> getActions(State q){
		return getActionsInSet(q,getActions());
	}
	
	/**
	 * detect whether the two sets of actions are disjoint
	 * @param A set of actions
	 * @param B set of actions
	 * @return if disjoint, return true
	 */
	private boolean bDisjoint(Set<Action> A, Set<Action> B){
		for(Action a: A){
			if(B.contains(a))
				return false;
		}
		return true;
	}
	
	/**
	 * For each transition tran=(q,a,q') where a is an input action, 
	 * if (q,a) not be a key of the map, add &lt;(q,a),tran&gt; to the map, 
	 * if &lt;(q,a),tran&gt; is already in the map, continue
	 * if the value of (q,a) in the map is not tran, 
	 * 		i.e. exists (q,a,q') and (q,a,q'') and q'!=q''
	 * 		indeterministic and return false
	 * @return the automaton is input-deterministic
	 */
	public boolean bInputDeterministic(){
		class KeyPair{
			@Override
			public int hashCode() {
				final int prime = 31;
				int result = 1;
				result = prime * result + ((a == null) ? 0 : a.hashCode());
				result = prime * result + ((s == null) ? 0 : s.hashCode());
				return result;
			}
			@Override
			public boolean equals(Object obj) {
				if (this == obj)
					return true;
				if (obj == null)
					return false;
				if (getClass() != obj.getClass())
					return false;
				KeyPair other = (KeyPair) obj;
				if (a == null) {
					if (other.a != null)
						return false;
				} else if (!a.equals(other.a))
					return false;
				if (s == null) {
					if (other.s != null)
						return false;
				} else if (!s.equals(other.s))
					return false;
				return true;
			}
			String s;
			String a;
			
			KeyPair(String s,String a){
				this.s=s;
				this.a=a;
			}
/*			public boolean equals(KeyPair kp){
				return (this.s.equals(kp.s) && this.a.equals(kp.a));
			}*/
		}
		Map<KeyPair, Transition> map=new HashMap<KeyPair, Transition>();
		for(Transition tran: this.getTransitionsByActions(this.getInActions())){
			KeyPair kp=new KeyPair(tran.getFr().getStr(),tran.getAct().getStr());
			Transition v=map.get(kp);
			if(v != null ){
				if(! v.equals(tran) )
					return false;
				else
					continue;
			}
			else{
				map.put(kp, tran);
			}
		}
		return true;
	}
	
	/**
	 * test if the current automaton is a valid one, 
	 * i.e. being input deterministic and the sets of actions are pairwisely disjoint
	 * @return true if the current automaton is valid
	 */
	public boolean bValid(){
		return (this.bInputDeterministic() &&
				bDisjoint(this.getInActions(),this.getOutActions()) &&
				bDisjoint(this.getInActions(),this.getHideActions()) &&
				bDisjoint(this.getHideActions(),this.getOutActions()));
	}
	
	/**
	 * @param q a specific state of automaton
	 * @return whether there is any transition issued from q
	 */
	public boolean bStateEnabled(State q){
		for(Transition tran:trans){
			if(tran.getFr().equals(q))
				return true;
		}
		return false;
	}
	
	/**
	 * @param q a specific state
	 * @param a a specific action
	 * @return whether there is a transition issued from q and perform the action a
	 */
	public boolean bStateActionEnabled(State q, Action a){
		for(Transition tran:trans){
			if(tran.getFr().equals(q) && tran.getAct().equals(a))
				return true;
		}
		return false;
	}
	
	/**
	 * @return whether the current automaton is closed (no input or output actions)
	 */
	public boolean bClosed(){
		return (this.getInActions().isEmpty() && this.getOutActions().isEmpty());
	}
	
	/**
	 * 
	 * @param sts1 a specific set of states
	 * @param sts2 a specific set of states
	 * @return the intersection of sts1 and sts2
	 */
	private Set<State> StateIntersection(Set<State> sts1, Set<State> sts2){
		Set<State> res=new HashSet<State>();
		for(State s: sts1){
			for(State t: sts2){
				if(s.getStr().equals(t.getStr()))
					res.add(s);
			}
		}
		return res;
	}
	
	/**
	 * 
	 * @param acts1 a specific set of actions
	 * @param acts2 a specific set of actions
	 * @return the intersection of acts1 and acts2
	 */
	private Set<Action> ActionIntersection(Set<Action> acts1, Set<Action> acts2){
		Set<Action> res=new HashSet<Action>();
		for(Action a: acts1){
			for(Action b: acts2){
				if(a.getStr().equals(b.getStr()) ){
					res.add(a);
				}
			}
		}
		return res;
	}
	/**
	 * @param ia a specific automaton
	 * @return the intersection of the actions of the current automaton and the actions of ia
	 */
	private Set<Action> SharedActions(InterfaceAutomaton ia){
		return ActionIntersection(this.getActions(),ia.getActions());
	}
	
	/**
	 * @param ia a specific automaton
	 * @return whether the current automaton and ia are composable
	 */
	public boolean bComposable(InterfaceAutomaton ia){
		return (ActionIntersection(this.getHideActions(),ia.getActions()).isEmpty() &&
		ActionIntersection(this.getActions(),ia.getHideActions()).isEmpty() &&
		ActionIntersection(this.getInActions(),ia.getInActions()).isEmpty() &&
		ActionIntersection(this.getOutActions(),ia.getOutActions()).isEmpty() );
	}
	
	/**
	 * @param s1 a specific set of states
	 * @param s2 a specific set of states
	 * @return the set of product states of s1 and s2
	 */
	private Set<State> StatesProduct(Set<State> s1, Set<State> s2){
		Set<State> res=new HashSet<State>();
		for(State p: s1){
			for(State q: s2){
				res.add(p.product(q));
			}
		}
		return res;
	}
	
	/**
	 * @param a a specific action
	 * @return the transitions whose action is a
	 */
	public Set<Transition> getTransitionsByAction(Action a){
		Set<Transition> res=new HashSet<Transition>();
		for(Transition tran: trans){
			if(tran.getAct().equals(a))
				res.add(tran);
		}
		return res;
	}
	
	/**
	 * @param ia a specific automaton
	 * @return the transitions of the product of the current automaton and ia
	 */
	private Set<Transition> TransitionsProduct(InterfaceAutomaton ia){
		Set<Action> shared=this.SharedActions(ia);
		Set<Transition> res=new HashSet<Transition>();
		
		for(Transition tran: trans){
			if(!shared.contains(tran.getAct()) ){
				for(State u:ia.getStates()){
					res.add(new Transition(tran.getFr().product(u),	tran.getAct(), tran.getTo().product(u)));
				}
			}
			else { //shared.contains(tran.getAct())
				for(Transition tran2: ia.getTransitions()){
					if(tran2.getAct().equals(tran.getAct()))
						res.add(new Transition(tran.getFr().product(tran2.getFr()), 
												tran.getAct(), tran.getTo().product(tran2.getTo()) ) );
				}
			}
		}
		for(Transition tran: ia.getTransitions()){
			if(!shared.contains(tran.getAct())){
				for(State u:this.getStates()){
					res.add(new Transition(u.product(tran.getFr()), tran.getAct(), u.product(tran.getTo())));
				}
			}
		}
		return res;
	}
	
	/**
	 * used in ReachableTrans().
	 * for each (q,a,q')\in trans, if q\in sts, then add q' to the result
	 * @param sts a set of states
	 * @param trans a set of transitions
	 * @return a set of states as the NEXT state of the states in sts, 
	 * 			directed by the transitions in trans
	 */
	public Set<State> nextStates(Set<State> sts, Set<Transition> trans){
		Set<State> res=new HashSet<State>();
		for(Transition tran: trans){
			if(sts.contains(tran.getFr()))
				res.add(tran.getTo());
		}
		return res;
	}
	/**
	 * @param visStates a set of states
	 * @param trans a set of transitions
	 * @return a subset of trans whose transitions are those issued from a state in visStates
	 */
	private Set<Transition> nextTransitions(Set<State> visStates, Set<Transition> trans){
		Set<Transition> res=new HashSet<Transition>();
		for(Transition tran: trans){
			if( visStates.contains(tran.getFr()) )
				res.add(tran);
		}
		return res;
	}
	
	/**
	 *  An auxiliary method used in ReachableStates()
	 */
	private Set<State> ReachableStatesAux(Set<State> vis_states, Set<Transition> trans){
		Set<State> tmpSts=nextStates(vis_states, trans);
		Set<Transition> tmpTrs=nextTransitions(vis_states, trans);
		if(tmpTrs.isEmpty())
			return vis_states;
		else{
			vis_states.addAll(tmpSts);
			trans.removeAll(tmpTrs);
			return ReachableStatesAux(vis_states,trans);
		}
	}
	/**
	 * A BFS to collect all the states reachable from the initial vis of states 
	 * @return the states reachable from the initial state
	 */
	public Set<State> ReachableStates(){
		Set<State> visState=new HashSet<State>();
		visState.add(this.init);
		Set<Transition> toFind=new HashSet<Transition>();
		toFind.addAll(this.trans);
		return ReachableStatesAux(visState,toFind);
	}
	
	/**
	 * an auxiliary method used in ReachableTrans()
	 */
	private Set<Transition> ReachableTransAux(Set<Transition> visTrans, Set<State> visStates, Set<Transition> toFind){
		Set<State> tmpSts=nextStates(visStates,toFind);
		Set<Transition> tmpTrs=nextTransitions(visStates,toFind);
		if(tmpTrs.isEmpty())
			return visTrans;
		else {
			visTrans.addAll(tmpTrs);
			visStates.addAll(tmpSts);
			toFind.removeAll(tmpTrs);
			return ReachableTransAux(visTrans, visStates, toFind);
		}
	}
	/**
	 * a BFS to find all the transitions reachable from the initial state
	 * @return the transitions reachable from the initial state of current automaton
	 */
	private Set<Transition> ReachableTrans(){
		Set<State> visStates=new HashSet<State>();
		visStates.add(this.init);
		Set<Transition> toFind=new HashSet<Transition>();
		toFind.addAll(this.trans);
		return ReachableTransAux(new HashSet<Transition>(), visStates, toFind);
	}
	
	/**
	 * @param trans a set of transitions
	 * @return the states involved with the transitions in trans
	 */
	private Set<State> getStatesByTransitions(Set<Transition> trans){
		Set<State> res=new HashSet<State>();
		for(Transition tran: trans){
			res.add(tran.getFr());
			res.add(tran.getTo());
		}
		return res;
	}
	
	/**
	 * @param ia a specific automaton
	 * @return the product of the current automaton and ia
	 */
	public InterfaceAutomaton Product(InterfaceAutomaton ia){
		InterfaceAutomaton res=new InterfaceAutomaton();
		res.states=StatesProduct(this.getStates(),ia.getStates());
		res.init=this.init.product(ia.init);
		
		Set<Action> shared=this.SharedActions(ia);
				
		res.ins.addAll(this.getInActions());
		res.ins.addAll(ia.getInActions());
		res.ins.removeAll(shared);
		
		res.outs.addAll(this.getOutActions());
		res.outs.addAll(ia.getOutActions());
		res.outs.removeAll(shared);
		
		res.hides.addAll(this.getHideActions());
		res.hides.addAll(ia.getHideActions());
		res.hides.addAll(shared);
		
		res.trans=this.TransitionsProduct(ia);
		return res;
	}
	/**
	 * @param ia a specific automaton
	 * @return the reduced product of the current automaton and ia
	 */
	public InterfaceAutomaton ReducedProduct(InterfaceAutomaton ia){
		InterfaceAutomaton tmpIA=this.Product(ia);
		tmpIA.trans=tmpIA.ReachableTrans();
		tmpIA.states=tmpIA.getStatesByTransitions(tmpIA.trans);
		return tmpIA;
	}
	
	/**
	 * @param ia a specific automaton
	 * @return the illegal states of the product this*ia
	 */
	public Set<State> Illegal(InterfaceAutomaton ia){
		Set<State> res=new HashSet<State>();
		Set<Action> shared=this.SharedActions(ia);
/*		for(Action a: shared){
			System.out.println("in Illegal: "+a.getStr());
		}*/
		for(State v:this.states){
			for(State u:ia.states){
				for(Action a: shared){
					if ( (this.getOutActions(v).contains(a) && !ia.getInActions(u).contains(a) ) ||
						(!this.getInActions(v).contains(a) && ia.getOutActions(u).contains(a)) )
						res.add(v.product(u));
				}
			}
		}
		return res;
	}
	
	/**
	 * @param env a specific automaton
	 * @return whether env is an environment of the current automaton
	 */
	public boolean bEnvironment(InterfaceAutomaton env){
		Set<Action> outs=this.getOutActions();
		Set<Action> ins=env.getInActions();
		return ( this.bComposable(env) && outs.containsAll(ins) & ins.containsAll(outs) && this.Illegal(env).isEmpty() );
	}
	
	/**
	 * used in the compatibility decision
	 * @param start a specific state
	 * @param targets a set of states
	 * @param trans a set of transitions
	 * @return whether the &lt;targets&gt; states are reachable from the state &lt;start&gt; through the transitions given in &lt;trans&gt;
	 */
	public boolean bTargetsReachableByTrans(State start, Set<State> targets, Set<Transition> trans){
		Set<State> vis=new HashSet<State>();
		vis.add(start);
		Set<State> reachables=ReachableStatesAux(vis,trans);
		if(! StateIntersection(reachables,targets).isEmpty() )
			return true;
		else
			return false;
	}
	
	/**
	 * @param acts a set of actions
	 * @return a subset of transitions whose actions are in acts
	 */
	private Set<Transition> getTransitionsByActions(Set<Action> acts){
		Set<Transition> res=new HashSet<Transition>();
		for(Transition trs: this.trans){
			if(acts.contains(trs.getAct())){
				res.add(trs);
			}
		}
		return res;
	}
	
	/**
	 * compatibility of IAs, 
	 * we implements Def.14 of deAlfaro'05. The older form (Def.8 of deAlfaro'01) is hard to implement.
	 * @param ia a specific automaton
	 * @return whether the current automaton and ia are compatible 
	 */
	public boolean bCompatible(InterfaceAutomaton ia){
		Set<State> error_states=this.Illegal(ia);
		
		InterfaceAutomaton prod=this.ReducedProduct(ia);
		
		Set<Action> tmpActs=new HashSet<Action>();
		tmpActs.addAll(prod.getOutActions());
		tmpActs.addAll(prod.getHideActions());
		Set<Transition> trs=prod.getTransitionsByActions(tmpActs);
		
		return (! bTargetsReachableByTrans(prod.getInit(), error_states, trs) );
	}
	
	/**
	 *  @return the states in srcs which cannot reach the targets by trs 
	 */
	private Set<State> getUnreachableSources(Set<State> srcs, Set<State> targets, Set<Transition> trs){
		Iterator<State> it=srcs.iterator();
		if(it.hasNext()){
			State s=it.next();
			it.remove();
			Set<State> tmpSrcs=getUnreachableSources(srcs, targets, trs);
			if(! bTargetsReachableByTrans(s, targets, trs) ) {
				tmpSrcs.add(s);
			}
			return tmpSrcs;
		} else {
			return new HashSet<State>();
		}
	}
	
	/**
	 * @param ia a specific automaton
	 * @return the set of compatible states of product this*ia
	 */
	private Set<State> CompatibleStatesOfProduct(InterfaceAutomaton ia){
		InterfaceAutomaton prod=this.ReducedProduct(ia);
		
		Set<Action> tmpActs=new HashSet<Action>();
		tmpActs.addAll(prod.getOutActions());
		tmpActs.addAll(prod.getHideActions());
		Set<Transition> trs=prod.getTransitionsByActions(tmpActs);
		
		Set<State> error_states=this.Illegal(ia);
		
		Set<State> srcs=new HashSet<State>();
		srcs.addAll(prod.getStates());
		srcs.removeAll(error_states);
		
		return getUnreachableSources(srcs,error_states,trs);
	}
	
	/**
	 * @param states a set of states
	 * @return the transitions whose states are in <states>
	 */
	private Set<Transition> getCloseTrans(Set<State> states){
		Set<Transition> res=new HashSet<Transition>();
		for(Transition trs: this.trans){
			if(states.contains(trs.getFr()) && states.contains(trs.getTo())){
				res.add(trs);
			}
		}
		return res;
	}
	
	/**
	 * the current automaton and ia are assumed to be compatible
	 * @param ia the right-side interface automaton for composition with current interface automaton
	 * @return the COMPOSITION of COMPATIBLE IAs
	 */
	public InterfaceAutomaton Composition(InterfaceAutomaton ia){
		InterfaceAutomaton prod=this.ReducedProduct(ia);
		Set<State> states=this.CompatibleStatesOfProduct(ia);
		Set<Transition> transitions=prod.getCloseTrans(states);
		
		prod.states=states;
		prod.trans=transitions;
		return prod;
	}
	
	/*used by the Composition2()*/
	private Set<State> OHPre(Set<State> U){
		Set<State> res=new HashSet<State>();
		Set<Action> tosearch=new HashSet<Action>();
		tosearch.addAll(this.getHideActions());
		tosearch.addAll(this.getOutActions());
		for(Transition t: this.trans){
			if(U.contains(t.getTo()) && tosearch.contains(t.getAct())){
				res.add(t.getFr());
			}
		}
		return res;
	}
	/**
	 * implementation of the Algorithm 1 of FSE'01
	 * @return U_{k+1} when U_k=U_{k+1}
	 */
	private Set<State> IncompatibleStates(Set<State> U){
		Set<State> tmp=OHPre(U);
		tmp.addAll(U);
		if( ! tmp.containsAll(U) || ! U.containsAll(tmp)){
			return IncompatibleStates(tmp);
		} else {
			return tmp;
		}
	}
	/**
	 * Composition using Algorithm 1 of FSE'01
	 * @param ia a specific automaton
	 * @return the composition of compatible IAs
	 */
	public InterfaceAutomaton Composition2(InterfaceAutomaton ia){
		InterfaceAutomaton prod=this.ReducedProduct(ia);
		prod.states.removeAll( prod.IncompatibleStates(this.Illegal(ia)) );
		prod.trans=prod.getCloseTrans(prod.states);
		return prod;
	}
	
	/**
	 * another definition of composition according to the Def.15 of Alfaro'05,
	 * incorrect because some unreachable transitions from init may be left
	 */
	/*public InterfaceAutomaton Composition2(InterfaceAutomaton ia){
		InterfaceAutomaton prod=this.ReducedProduct(ia);
		Set<State> states=this.CompatibleStatesOfProduct(ia);
		
		Set<Transition> to_del=new HashSet<Transition>();
		for(Transition trs: prod.getTransitions()){
			if(states.contains(trs.getFr()) && 
					! states.contains(trs.getTo()) && 
					prod.getInActions().contains(trs.getAct()) )
				to_del.add(trs);
		}
		prod.trans.removeAll(to_del);
		return prod;
	}*/
	
	/**
	 * @param q using q to generate the epsilon-closure of q
	 * @return the set of states w.r.t. the epsilon-closure of q
	*/
	public Set<State> EpsilonClosure(State q){
		Set<Transition> hidden_trans=getTransitionsByActions(this.getHideActions());
		Set<State> init=new HashSet<State>();
		init.add(q);
		return ReachableStatesAux(init,hidden_trans);
	}
	
	/** for each state u in EpsilonClosure(q), find the outsourcing input/output actions from u.
	 * Note that the obsAO(q) find all the outputs from ANY u in EpsilonClosure(q), 
	 * while obsAI(q) find the inputs from ALL u in EpsilonClosure(q) 
	 */
	private Set<Action> obsAO(State q){
		Set<Action> res=new HashSet<Action>();
		for(State r: EpsilonClosure(q)){
			res.addAll( this.getOutActions(r) );
		}
		return res;
	}
	private Set<Action> obsAI(State q){
		Set<Action> res=new HashSet<Action>();
		res.addAll(this.getActions());
		for(State r: EpsilonClosure(q)){
			res=ActionIntersection(res,this.getInActions(r));
		}
/*		System.out.println("obsAI:");
		for(Action a: res){
			System.out.println(a.getStr());
		}*/
		return res;
	}
	
	public Set<State> post(State q, Action a){
		Set<State> res=new HashSet<State>();
		for(Transition trs: this.trans){
			if(trs.getFr().equals(q) && trs.getAct().equals(a)){
				res.add(trs.getTo());
			}
		}
		return res;
	}
	
	/**
	 * this alternating simulation is an implementation on Def 19 of de Alfaro'05,
	 * also takes the principle of Algorithm 2 of FSE'01
	 * @param ia a specific automaton
	 * @param relation the current relation set
	 * @return whether the current automaton and ia satisfy the alternating simulation relation
	 */
	public Rel MaxAlterSim(InterfaceAutomaton ia, Rel relation){
		Rel relset=new Rel();
Label:	for(StatePair sp: relation.rel){
			State q=sp.p;
			State q_=sp.q;
			for(Action a: this.getInActions(q)){
				for(State r: this.post(q,a)){
					boolean r_correct=false;
					for(State r_: ia.post(q_,a)){
						if(relation.rel.contains(new StatePair(r,r_)) ){
							r_correct=true;
							break;
						}
					}
					if(! r_correct){
						continue Label;
					}
				}
			}//Here: Definition 19 (1) is satisfied
			for(Action a: ia.getOutActions(q_)){
				for(State r_: ia.post(q_, a)){
					boolean r2_correct=false;
Label2:				for(State p: this.EpsilonClosure(q)){
						for(State r:post(p,a)){
							if(relation.rel.contains(new StatePair(r,r_)) ){
								r2_correct=true;
								break Label2;
							}
						}
					}
					if(! r2_correct){
						continue Label;
					}
				}
			}//Here: Def 19 (2) is satisfied
			for(Action a: ia.getHideActions(q_)){
				for(State r_: ia.post(q_, a)){
					boolean r3_correct=false;
					for(State r: this.EpsilonClosure(q)){
						if(relation.rel.contains(new StatePair(r,r_)) ){
							r3_correct=true;
							break;
						}
					}
					if(! r3_correct){
						continue Label;
					}
				}
			}//Here Def 19 (3) is satisfied
			relset.add(sp);
		}
		if(! relset.rel.containsAll(relation.rel) || ! relation.rel.containsAll(relset.rel)){
			//relation=relset;
			return MaxAlterSim(ia,relset);
		}
		else{
			return relation;
		}
	}
	
	public boolean bRefinedBy(InterfaceAutomaton ia){
		Rel relation=new Rel();
		for(State s: this.states){
			for(State t: ia.states){
				relation.add(new StatePair(s,t));
			}
		}
		return ( ia.getInActions().containsAll(this.getInActions())
				&& this.getOutActions().containsAll(ia.getOutActions())
				&& this.MaxAlterSim(ia,relation).rel.contains(new StatePair(this.init,ia.init)));
	}
		
	//another definition of alternating simulation, FSE'01
	private Set<State> ExtDest(State v, Action a){
		Set<State> res=new HashSet<State>();
		Set<Action> ioacts=new HashSet<Action>();
		ioacts.addAll(this.obsAI(v));
		ioacts.addAll(this.obsAO(v));
		if(! ioacts.contains(a)){
			System.out.println("ExtDest: action a is neither in obsAI nor obsAO");
			return res;
		}
		for(State u: this.EpsilonClosure(v)){
			res.addAll(post(u,a) );
		}
		return res;
	}
	
	/**
	 * another definition of alternating simulation, Algorithm 2 of FSE'01
	 * @param ia a specific automaton
	 * @param relation the current relation set
	 * @return whether the current automaton and ia satisfy the alternating simulation relation
	 */
	public Rel MaxAlterSim2(InterfaceAutomaton ia, Rel relation){
		Rel relset=new Rel();
Label:	for(StatePair sp: relation.rel){
			State v=sp.p;
			State u=sp.q;
			
			Set<Action> acts1=this.obsAI(v);
			Set<Action> acts2=ia.obsAI(u);
			for(Action a: acts1){
				if(! acts2.contains(a)){ //this.obsAI(v) not a subset of ia.obsAI(u)
					continue Label;
				}
			}
			acts1=this.obsAO(v);
			acts2=ia.obsAO(u);
			for(Action a: acts2){
				if(! acts1.contains(a)){ //ia.obsAO(u) not a subset of this.obsAO(v)
					continue Label;
				}
			}
			acts2.addAll(this.obsAI(v));//now acts2 contains ExtEn^I_P(v)\cup ExtEn^O_Q(u)
			if(acts2.isEmpty()){
				relset.add(sp);
				continue Label;
			}
			
			for(Action a: acts2){
				Set<State> dests=ia.ExtDest(u,a);
				Set<State> dests2=this.ExtDest(v,a);
				if(dests.isEmpty())
					continue;
				if(dests2.isEmpty()){
					continue Label;
				}
				for(State u_: dests){
					boolean b=false;
					for(State v_: dests2){
						if(relation.rel.contains(new StatePair(v_,u_))){
							b=true;
							break;
						}
					}
					if(! b){
						continue Label;
					}
				}
			}
			relset.add(sp);
		}
		if(! relset.rel.containsAll(relation.rel) || ! relation.rel.containsAll(relset.rel)){
			relation=relset;
			return MaxAlterSim2(ia,relation);
		}
		else{
			return relation;
		}
	}
	
	public boolean bRefinedBy2(InterfaceAutomaton ia){
		Rel relation=new Rel();
		for(State s: this.states){
			for(State t: ia.states){
				relation.add(new StatePair(s,t));
			}
		}
/*		for(StatePair sp: this.MaxAlterSim(ia, relation).rel)
			System.out.print("<"+sp.p.getStr()+","+sp.q.getStr()+">;");
		System.out.println();*/
		return ( ia.getInActions().containsAll(this.getInActions()) )
				&& ( this.getOutActions().containsAll(ia.getOutActions()) )
				&& ( (this.MaxAlterSim2(ia, relation) ).rel.contains(new StatePair(this.init,ia.init)) );
	}
	
	/**
	 * Print the current automaton
	 */
	public void Print(){
		StringBuilder tmp=new StringBuilder();
		tmp.append("*******************************\n");
		tmp.append("IA [");
		for(State s: this.states)
			tmp.append(s.getStr()+"," );
		tmp.deleteCharAt(tmp.length()-1);
		tmp.append("] ("+this.init.getStr()+")\n [");
		for(Action a: this.ins){
			tmp.append(a.getStr()+",");
		}
		if(tmp.charAt(tmp.length()-1)==',')
			tmp.deleteCharAt(tmp.length()-1);
		tmp.append("] [");
		for(Action a: this.outs){
			tmp.append(a.getStr()+",");
		}
		if(tmp.charAt(tmp.length()-1)==',')
			tmp.deleteCharAt(tmp.length()-1);
		tmp.append("] [");
		for(Action a: this.hides){
			tmp.append(a.getStr()+",");
		}
		if(tmp.charAt(tmp.length()-1)==',')
			tmp.deleteCharAt(tmp.length()-1);
		tmp.append("]\n [");
		for(Transition t: this.trans){
			tmp.append("("+t.getFr().getStr()+","+ t.getAct().getStr()+","+ t.getTo().getStr()+"),");
		}
		if(tmp.charAt(tmp.length()-1)==',')
			tmp.deleteCharAt(tmp.length()-1);
		tmp.append("]\n");
		tmp.append("*******************************\n");
		System.out.println(tmp.toString());
	}

	/**
	 * Print the current automaton in the input language
	 */
	public void Print2(){
		System.out.println(this.generateIAOutputString(this.getName()));
	}
	
	/**
	 * Generate an output string for automata. The string can be parsed back by the IAParser
	 */
	private boolean validCharacter(char c){
		if( (c<='Z' && c>='A') ||
			(c<='z' && c>='a') ||
			(c<='9' && c>='0') ||
			c=='_'){
			return true;
		}
		return false;
	}
	private String StrRegulate(String s){
		StringBuilder sb=new StringBuilder();
		if(s.charAt(0)>='0' && s.charAt(0)<='9')
			sb.append('_');
		for(int i=0;i<s.length();i++){
			char c= s.charAt(i);
			if (!validCharacter(c))
				sb.append('_');
			else
				sb.append(s.charAt(i));
		}
		return sb.toString();
	}
	
/*	private void validateIASymbols(InterfaceAutomaton ia){
		ia.setInit(StrRegulate(ia.getInit().getStr()));
		for(Action a: ia.getInActions()){
			a.
		}
	}*/
	public String generateIAOutputString(String module_name){
		StringBuilder sb=new StringBuilder();
		sb.append("----------------- " + module_name + " --------------------\n");
		sb.append("module ").append(module_name==null? "": StrRegulate(module_name)).append(":\n");
		sb.append("initial : ").append(StrRegulate(this.init.getStr())).append("\n");
		
		//Map<Transition,String> map=new HashMap<String,Transition>();
		for(Action a: this.getActions()){
			Set<Transition> tmpTrs=new HashSet<Transition>();
			for(Transition tr: this.trans){
				if(tr.getAct().equals(a))
					tmpTrs.add(tr);
			}
			if(this.getInActions().contains(a))
				sb.append("input ");
			else if(this.getOutActions().contains(a))
				sb.append("output ");
			else
				sb.append("hide ");
			sb.append(StrRegulate(a.getStr())).append(" : { ");
			if(!tmpTrs.isEmpty()){
				for(Transition tr: tmpTrs){
					sb.append( StrRegulate(tr.getFr().getStr()) )
						.append(" -> ").append( StrRegulate(tr.getTo().getStr()) ).append(",");
				}
				sb.setCharAt(sb.length()-1, '}');
				sb.append("\n");
			}
			else {
				sb.append("}\n");
			}
		}		
		sb.append("endmodule\n");
		sb.append("-------------------------------------------------------\n");
		return sb.toString();
	}

	public String generateIAOutputString2(String module_name){
		StringBuilder sb=new StringBuilder();
		sb.append("module ").append(module_name==null? "": StrRegulate(module_name)).append(":\n");
		sb.append("initial : ").append(StrRegulate(this.init.getStr())).append("\n");
		
		//Map<Transition,String> map=new HashMap<String,Transition>();
		for(Action a: this.getActions()){
			Set<Transition> tmpTrs=new HashSet<Transition>();
			for(Transition tr: this.trans){
				if(tr.getAct().equals(a))
					tmpTrs.add(tr);
			}
			if(this.getInActions().contains(a))
				sb.append("input ");
			else if(this.getOutActions().contains(a))
				sb.append("output ");
			else
				sb.append("hide ");
			sb.append(StrRegulate(a.getStr())).append(" : { ");
			if(!tmpTrs.isEmpty()){
				for(Transition tr: tmpTrs){
					sb.append( StrRegulate(tr.getFr().getStr()) )
						.append(" -> ").append( StrRegulate(tr.getTo().getStr()) ).append(",");
				}
				sb.setCharAt(sb.length()-1, '}');
				sb.append("\n");
			}
			else {
				sb.append("}\n");
			}
		}		
		sb.append("endmodule\n");
		return sb.toString();
	}
	
	//these inner classes used in the alternating simulation
	class StatePair{
		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + ((p == null) ? 0 : p.hashCode());
			result = prime * result + ((q == null) ? 0 : q.hashCode());
			return result;
		}
		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			StatePair other = (StatePair) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (p == null) {
				if (other.p != null)
					return false;
			} else if (!p.equals(other.p))
				return false;
			if (q == null) {
				if (other.q != null)
					return false;
			} else if (!q.equals(other.q))
				return false;
			return true;
		}
		private State p;
		private State q;
		public StatePair(State p,State q){
			this.p=p;
			this.q=q;
		}
/*		public boolean equal(StatePair sp){
			return (this.p.equals(sp.p) && this.q.equals(sp.q));
		}*/
		private InterfaceAutomaton getOuterType() {
			return InterfaceAutomaton.this;
		}
		
	}
	class Rel{
		private Set<StatePair> rel=new HashSet<StatePair>();
		
		public boolean add(StatePair sp){
			return rel.add(sp);
		}
	}
}
