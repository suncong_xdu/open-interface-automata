/*
 * This software was developed by Cong Sun <suncong@xidian.edu.cn>
 * at Xidian University, China.
 *
 * This file is part of Open Interface Automata.
 * Open Interface Automata is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Open Interface Automata is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Open Interface Automata.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.xdu.jia;

public class Transition{
	private State s;
	private Action a;
	private State t;
	
	public Transition(String s,String a,String t){
		this.s=new State(s);
		this.a=new Action(a);
		this.t=new State(t);
	}
	public Transition(State s,Action a,State t){
		this.s=s;
		this.a=a;
		this.t=t;
	}
	
	public State getFr(){
		return s;
	}
	public Action getAct(){
		return a;
	}
	public State getTo(){
		return t;
	}
/*	public boolean equals(Transition tran){
		return (this.s.equals(tran.getFr()) && this.a.equals(tran.getAct()) && this.t.equals(tran.getTo()) );
	}*/
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((a == null) ? 0 : a.hashCode());
		result = prime * result + ((s == null) ? 0 : s.hashCode());
		result = prime * result + ((t == null) ? 0 : t.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Transition other = (Transition) obj;
		if (a == null) {
			if (other.a != null)
				return false;
		} else if (!a.equals(other.a))
			return false;
		if (s == null) {
			if (other.s != null)
				return false;
		} else if (!s.equals(other.s))
			return false;
		if (t == null) {
			if (other.t != null)
				return false;
		} else if (!t.equals(other.t))
			return false;
		return true;
	}

}
