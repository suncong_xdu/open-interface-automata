/*
 * This software was developed by Cong Sun <suncong@xidian.edu.cn>
 * at Xidian University, China.
 *
 * This file is part of Open Interface Automata.
 * Open Interface Automata is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.

 * Open Interface Automata is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Open Interface Automata.  If not, see <https://www.gnu.org/licenses/>.
 */

package org.xdu.jia;

public class Action{
	private String a;
	
	public Action(String a){
		this.a=a;
	}
	public String getStr(){
		return a;
	}
/*	public boolean equals(Action a){
		return this.a.equals(a.getStr());
	}*/
	
    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((a == null) ? 0 : a.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Action other = (Action) obj;
		if (a == null) {
			if (other.a != null)
				return false;
		} else if (!a.equals(other.a))
			return false;
		return true;
	}
}
