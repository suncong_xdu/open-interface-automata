#!/bin/bash

#generate source code of the parser
java -jar lib/sablecc.jar grammar/IA.grammar
cp -r grammar/org/ src/ && rm -r grammar/org

#build the source code
cd src
make
mv javaia.jar ../lib/
make clean

#generate java docs of interface automata library
javadoc -subpackages -public -Xdoclint:html,syntax,accessibility org.xdu.jia -d ../doc/

