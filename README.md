# Open Interface Automata

This is an implementation of interface automata[1][2] in Java.

The library takes either a string or some textual automata model in IA.grammar to build the interface automata. The textual language of automata is a simplified version of the language of Ticc [3]. For more usage of interface automata APIs, please refer to the document of class *InterfaceAutomaton*.

---

## Usage

* Run 
```
./deploy.sh
```
to build the *javaia.jar* into *lib/*.

* To use the library, please import *lib/javaia.jar* into your eclipse project, and refer to *checker/IAValidate* to develop your own program.

---

## References

1. Luca de Alfaro, Thomas A. Henzinger: Interface automata. ESEC / SIGSOFT FSE 2001: 109-120
2. Luca de Alfaro, Thomas A. Henzinger: Interface-Based Design. In: Broy M., Grünbauer J., Harel D., Hoare T. (eds) Engineering Theories of Software Intensive Systems. NATO Science Series (Series II: Mathematics, Physics and Chemistry), vol 195. Springer, Dordrecht 83-104.
3. B. Thomas Adler, Luca de Alfaro, Leandro Dias da Silva, Marco Faella, Axel Legay, Vishwanath Raman, Pritam Roy: Ticc: A Tool for Interface Compatibility and Composition. CAV 2006: 59-62.
4. Cong Sun, Ning Xi, Jinku Li, Qingsong Yao, Jianfeng Ma: Verifying Secure Interface Composition for Component-Based System Designs. APSEC (1) 2014: 359-366

## Author

* **Cong Sun** - *School of Cyber Engineering, Xidian University* - [E-mail: suncong AT xidian DOT edu DOT cn]

## License

* This library is licensed under the GPL 3.0 License. If it is used for academic publication, please cite our work [4].
